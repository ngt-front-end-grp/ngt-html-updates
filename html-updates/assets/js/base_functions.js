

function to_time (seconds) 
{
    var days    = Math.floor(seconds / 86400);
    var hours   = Math.floor((seconds - (days * 86400)) / 3600);
    var minutes = Math.floor((seconds - (days * 86400) - (hours * 3600)) / 60);
    var seconds = seconds - (days * 86400) - (hours * 3600) - (minutes * 60);
    var time = "";

    if (days !== 0)  time = days+"d ";

    if (hours !== 0)  time += hours+"h ";
    
    if (minutes !== 0 || time !== "") 
    {
      minutes = (minutes < 10 && time !== "") ? "0"+minutes : String(minutes);
      time += minutes+"min ";
    }
    
    if (seconds !== 0) time += seconds+"s";
    return time;
}

